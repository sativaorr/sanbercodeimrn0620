//Soal No. 1 (Range)
console.log("Soal No. 1");
console.log("===========================");

function range(startNum, finishNum) {
    arr = [];
    if (startNum == undefined || finishNum == undefined) {
        return -1;
    } else if (startNum < finishNum) {
        for (x = startNum; x <= finishNum; x++) {
            arr.push(x);
        }
        return arr;
    } else if (startNum > finishNum) {
        for (x = startNum; x >= finishNum; x--) {
            arr.push(x);
        }
        return arr;
    }

}
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11, 18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 


console.log(" ");

//Soal No. 2 (Range with Step)
console.log("Soal No. 2");
console.log("===========================");

function rangeWithStep(startNum, finishNum, step) {
    arr = [];
    if (startNum == undefined || finishNum == undefined) {
        return -1;
    } else if (startNum < finishNum) {
        for (x = startNum; x <= finishNum; x += step) {
            arr.push(x);
        }
        return arr;
    } else if (startNum > finishNum) {
        for (x = startNum; x >= finishNum; x -= step) {
            arr.push(x);
        }
        return arr;
    }
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

console.log(" ");

//Soal No. 3 (Sum of Range)
console.log("Soal No. 3");
console.log("===========================");

function sum(startNum = 0, finishNum = 0, step = 1) {
    arr = [];
    if (startNum == 0) {
        return 0;
    } else if (startNum < finishNum) {
        for (x = startNum; x <= finishNum; x += step) {
            arr.push(x);
        }
        sumarr = arr.reduce((a, b) => a + b, 0);
        return sumarr;
    } else if (startNum > finishNum) {
        for (x = startNum; x >= finishNum; x -= step) {
            arr.push(x);
        }
        sumarr = arr.reduce((a, b) => a + b, 0);
        return sumarr;
    }
}

console.log(sum(1, 10)); // 55
console.log(sum(5, 50, 2)); // 621
console.log(sum(15, 10)); // 75
console.log(sum(20, 10, 2)); // 90
console.log(sum(1)); // 1
console.log(sum()); // 0 

console.log(" ")

//Soal No. 4 (Array Multidimensi)
console.log("Soal No. 4");
console.log("===========================");

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

function dataHandling() {
    for (var i = 0; i < input.length; i++) {
        console.log("Nomor ID:  " + input[i][0]);
        console.log("Nama Lengkap:  " + input[i][1]);
        console.log("TTL:  " + input[i][2] + " " + input[i][3]);
        console.log("Hobi:  " + input[i][4]);
        console.log(" ");
    }
}
dataHandling();

//Soal No. 5 (Balik Kata)
console.log("Soal No. 5");
console.log("===========================");

function balikKata(str) {
    var currentString = str;
    var newString = '';
    for (let i = str.length - 1; i >= 0; i--) {
        newString = newString + currentString[i];
    }

    return newString;
}
console.log(balikKata("Kasur Rusak")); // kasuR rusaK
console.log(balikKata("SanberCode")); // edoCrebnaS
console.log(balikKata("Haji Ijah")); // hajI ijaH
console.log(balikKata("racecar")); // racecar
console.log(balikKata("I am Sanbers")); // srebnaS ma I 

console.log(" ");

//Soal No. 6 (Metode Array)
console.log("Soal No. 6");
console.log("===========================");

function dataHandling2() {
    input.splice(4, 4, 'Pria', 'SMA Internasional Metro');
    tambahNama = input[1] + 'Elshrawy';
    tambahprov = 'Provinsi ' + input[2]
    
    input[1] = tambahNama
    input[2] = tambahprov
    var tanggal = input[3].split("/")
    
    var bulan = tanggal[1]
    switch (bulan) {
        case '01':
            bulan = 'Januari'
            break;

        case '02':
            bulan = 'Februari'
            break;
        case '03':
            bulan = 'Maret'
            break;
        case '04':
            bulan = 'April'
            break;
        case '05':
            bulan = 'Mei'
            break;
        case '06':
            bulan = 'Juni'
            break;
        case '07':
            bulan = 'Juli'
            break;
        case '08':
            bulan = 'Agustus'
            break;
        case '09':
            bulan = 'September'
            break;
        case '10':
            bulan = 'Oktober'
            break;
        case '11':
            bulan = 'November'
            break;
        case '12':
            bulan = 'Desember'
            break;
    }
    var jointgl = tanggal.join("-");
    var srttgl = tanggal.sort(function(a, b){return b - a});
    var nama = input[1].toString().substr(0,14);
    console.log(input);
    console.log(bulan);
    console.log(srttgl);
    console.log(jointgl)
    console.log(nama);

}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);
