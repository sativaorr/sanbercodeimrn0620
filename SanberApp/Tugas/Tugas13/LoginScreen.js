import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    TouchableOpacity
} from 'react-native';

export default class LoginScreen extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Image source={require('./images/logo.png')} />
                <Text style={styles.title}>Login</Text>
                <View style={styles.body}>
                    <Text style={styles.inputInfo}>Username/Email</Text>
                    <TextInput style={styles.inputField} placeholder="Username/email" />
                    <Text style={styles.inputInfo}>Username/Email</Text>
                    <TextInput style={styles.inputField} placeholder="Username/email" />
                    <TouchableOpacity
                        style={styles.buttonLogin}
                        onPress={() => this.onPress()}
                    >
                        <Text style={styles.textButton}>Masuk</Text>
                    </TouchableOpacity>
                    <Text style={styles.text}>atau</Text>
                    <TouchableOpacity
                        style={styles.buttonDaftar}
                        onPress={() => this.onPress()}
                    >
                        <Text style={styles.textButton}>Daftar!</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    logo: {
        width: 375,
        height: 102,
        marginTop: 62,
        alignSelf: "center"
    },
    title: {
        fontSize: 24,
        fontFamily: 'Roboto',
        marginTop: 70,
        alignSelf: "center",
        color: '#003366'
    },
    body: {
        marginTop: 40,
        alignSelf: "center"
    },
    inputInfo: {
        fontSize: 13,
        textAlign: "left",
        color: '#003366'
    },
    inputField: {
        borderWidth: 1,
        borderColor: "#003366",
        width: 294,
        padding: 8,
        marginVertical: 10
    },
    buttonLogin:{
        alignSelf: "center",
        alignItems: "center",        
        marginTop: 38,
        width: 140,
        height: 40,
        borderRadius: 16,
        backgroundColor  : "#3EC6FF"
    },
    textButton:{
        fontSize: 24,
        alignSelf: "center",
        alignItems: "center",
        color: "#ffffff",
        fontFamily: 'Roboto',
        
    },
    text:{
        alignSelf: "center",
        margin: 16,
        color: "#3EC6FF",
        fontSize: 24,
        fontFamily: 'Roboto'

    },
    buttonDaftar:{
        alignSelf: "center",
        alignItems: "center",        
        width: 140,
        height: 40,
        borderRadius: 16,
        backgroundColor  : "#003366FF"
    }
});