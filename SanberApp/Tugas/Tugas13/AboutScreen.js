import React, { Component } from 'react'
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    TextInput,
    TouchableOpacity
} from 'react-native';
import { FontAwesome } from '@expo/vector-icons';

export default class AboutScreen extends Component {

    render() {
        return (
            <ScrollView style={styles.container}>
                <Text style={styles.title}>Tentang Saya</Text>
                <View style={styles.photos}>
                    <FontAwesome name="user" size={170} color="#CACACA" />
                </View>
                <Text style={styles.nama}>A. Chondro</Text>
                <Text style={styles.desc}>React Native Developer</Text>
                <View style={styles.portofolio}>
                    <Text style={styles.titlebox}>Portofolio</Text>
                    <View style={styles.layout} >
                        <View style={styles.item} >
                            <FontAwesome name="gitlab" size={40} color="#3EC6FF" />
                            <Text style={styles.descitem}>@sativaorr</Text>
                        </View>
                        <View style={styles.item} >
                            <FontAwesome name="github" size={40} color="#3EC6FF" />
                            <Text style={styles.descitem}>@sativaorr</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.contact}>
                    <Text style={styles.titlebox}>Hubungi Saya</Text>
                    <View style={styles.layout2} >
                        <View style={styles.item2} >
                            <FontAwesome name="facebook" size={40} color="#3EC6FF" />
                            <Text style={styles.descitem}>@sativaorr</Text>
                        </View>
                        <View style={styles.item2} >
                            <FontAwesome name="instagram" size={40} color="#3EC6FF" />
                            <Text style={styles.descitem}>@sativaorr</Text>
                        </View>
                        <View style={styles.item2} >
                            <FontAwesome name="twitter" size={40} color="#3EC6FF" />
                            <Text style={styles.descitem}>@sativaorr</Text>
                        </View>
                    </View>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    title: {
        fontSize: 36,
        fontWeight: "bold",
        fontFamily: 'Roboto',
        marginTop: 64,
        alignSelf: "center",
        color: '#003366'
    },
    photos: {
        marginTop: 12,
        paddingTop: 12,
        width: 200,
        height: 200,
        borderRadius: 100,
        alignSelf: "center",
        alignItems: "center",
        backgroundColor: "#EFEFEF"
    },
    nama: {
        marginTop: 24,
        alignSelf: "center",
        fontSize: 24,
        fontWeight: "bold",
        fontFamily: 'Roboto',
        color: '#003366'

    },
    desc: {
        marginTop: 8,
        alignSelf: "center",
        fontSize: 16,
        fontWeight: "bold",
        fontFamily: 'Roboto',
        color: '#3EC6FF'

    },
    portofolio: {
        marginHorizontal: 8,
        marginTop: 16,
        width: 340,
        height: 140,
        alignSelf: "center",
        backgroundColor: "#EFEFEF",
        borderRadius: 16

    },
    contact: {
        marginHorizontal: 8,
        marginTop: 9,
        width: 340,
        height: 300,
        alignSelf: "center",
        backgroundColor: "#EFEFEF",
        borderRadius: 16,
        marginBottom: 20

    },
    titlebox: {
        fontSize: 18,
        fontWeight: "normal",
        fontFamily: 'Roboto',
        margin: 5,
        textAlign: "left",
        color: '#003366',
        borderBottomWidth: 1,
        borderBottomColor: "#003366"
    },
    layout: {
        flex: 1,
        flexDirection: 'row',

    },
    item:{
        alignItems: "center",
        paddingTop: 20,
        marginHorizontal:45


    },
    descitem:{
        marginTop: 8,
        alignSelf: "center",
        fontSize: 16,
        fontWeight: "bold",
        fontFamily: 'Roboto',
        color: '#003366'
    },
    layout2: {
        flex: 1,
        flexDirection: 'column',

    },
    item2:{
        flex:1,
        flexDirection: 'row',
        alignItems: "center",
        paddingTop: 10,
        marginHorizontal:45


    }
});