//Soal 1 Looping
console.log("Soal No. 1")
console.log("===========================")
var x = 1;
console.log("LOOPING PERTAMA")
while (x <= 20) {
    if (x % 2 == 0) {
        console.log(x + ' - I love coding');
    }
    x++;
}
console.log("LOOPING KEDUA")
while (x >= 1) {
    if (x % 2 == 0) {
        console.log(x + ' - I will become a mobile developer');
    }
    x--;
}

console.log(" ")

//Soal 2 Looping menggunakan for
console.log("Soal No. 2")
console.log("===========================")

var i = 1;
for (i; i <= 20; i++) {
    if (i % 2 == 0) {
        console.log(i + " - Berkualitas");
    } else if (i % 2 == 1 && i % 3 == 0) {
        console.log(i + " - I Love Coding");
    } else {
        console.log(i + " - Santai");
    }
}

console.log(" ")

//No. 3 Membuat Persegi Panjang
console.log("Soal No. 3")
console.log("===========================")

for (var i = 1; i <= 4; i++) {

    var str = '';

    for (var j = 1; j <= 8; j++) {
        str += '#';
    }
    console.log(str);
}

console.log(" ")

//No. 4 Membuat Tangga
console.log("Soal No. 4")
console.log("===========================")

for (var i = 1; i <= 7; i++) {

    var str = '';

    for (var j = 1; j <= i; j++) {
        str += '#';
    }
    console.log(str);
}

console.log(" ")

//No. 5 Membuat Papan Catur
console.log("Soal No. 5")
console.log("===========================")

for (var i = 1; i <= 8; i++) {

    var catur = '';

    for (var j = 1; j <= 8; j++) {
        if ((j % 2 == 0 && i % 2 == 0) || (i % 2 == 1 && j % 2 == 1)) {
            catur += ' ';
        } else {
            catur += '#';
        }
    }
    console.log(catur);
}

console.log(" ")