// di index.js
var readBooks = require('./callback.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]

var i = 0;
function bacabuku(times) {
    if (i < books.length) {
        readBooks(times, books[i], sisaWaktu => bacabuku(sisaWaktu))
    }
    i++
}
bacabuku(10000)