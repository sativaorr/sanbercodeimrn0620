var readBooksPromise = require('./promise.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]

var i = 0;
function bacabuku2(times) {
    if (i < books.length) {
        readBooksPromise(times, books[i])
            .then(hasil => bacabuku2(hasil))
            .catch(error => console.log(error))
    }
    i++
}
bacabuku2(10000)