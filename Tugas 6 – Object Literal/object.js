//Soal No. 1 (Array to Object)
console.log("Soal No. 1");
console.log("===========================");

function arrayToObject(arr) {
    {
        var nama;
        var year = (new Date()).getFullYear();

        for (let i = 0; i < arr.length; i++) {
            var objek = {};

            objek.firstName = arr[i][0];
            objek.lastName = arr[i][1];
            objek.gender = arr[i][2];

            if (year > arr[i][3]) {
                objek.age = year - arr[i][3];
            } else {
                objek.age = 'Invalid Birth Year';
            }

            nama = (i + 1) + '. ' + objek.firstName + ' ' + objek.lastName + ':\n';

            console.log(nama, objek);
        }
    }
}

// Driver Code
var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]]
arrayToObject(people)
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]]
arrayToObject(people2)
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

// Error case 
arrayToObject([]) // ""

console.log(" ")

//Soal No. 2 (Shopping Time)
console.log("Soal No. 2");
console.log("===========================");

function shoppingTime(memberId, money) {
    // you can only write your code here!
    var memberMon = (memberId, money)
    var memberID = memberId
    // var sisaUang = money
    var belanjaan = []
    var totalPrice = 0;
    var arrItem = ['Sepatu Stacattu', 'Baju Zoro', 'Baju H&N', 'Sweater Uniklooh', 'Casing Handphone']
    var arrHarga = [1500000, 500000, 250000, 175000, 50000]

    if (memberID === '' || memberMon === undefined) {
        return ("Mohon maaf, toko X hanya berlaku untuk member saja")
    } else if (money < 50000) {
        return ("Mohon maaf, uang tidak cukup")
    } else {
        for (var i = 0; i < 5; i++) {
            //
            if (money >= arrHarga[i]) {
                totalPrice = totalPrice + arrHarga[i]
                //console.log(totalPrice)
                changeMoney = money - totalPrice
                //console.log(changeMoney)
                belanjaan.push(arrItem[i])
                // console.log(belanjaan)
            }
        }
        var shopObj = {}
        shopObj.memberId = memberId
        shopObj.money = money
        shopObj.listPurchased = belanjaan
        shopObj.changeMoney = changeMoney
    }
    return shopObj
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

console.log(" ")


//Soal No. 3 (Naik Angkot)
console.log("Soal No. 3");
console.log("===========================");

function naikAngkot(arrPenumpang) {

    rute = ['A', 'B', 'C', 'D', 'E', 'F'];

    var ar = [];
    var obj = {};

    for (var i = 0; i < arrPenumpang.length; i++) {
        obj = {};
        for (var j = 0; j < rute.length; j++) {
            if (arrPenumpang[i][2] === rute[j]) {
                var besar = j;
            } else if (arrPenumpang[i][1] === rute[j]) {
                var kecil = j;
            }
        }

        var bayaran = (besar - kecil) * 2000;

        obj.penumpang = arrPenumpang[i][0];
        obj.naikDari = arrPenumpang[i][1];
        obj.tujuan = arrPenumpang[i][2];
        obj.bayar = bayaran;

        ar.push(obj);
    }

    return ar;
}

//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]